//function to generate AAM auth tokens for all 4 AEC instances
function getAAMtokens() {
  
  var username = "";
  var password = "Momo1792$adobe";
  var tokens = {};
  
  //array of usernames for EMEA, APAC, NA, LATAM
  var regions = ["EMEA", "APAC", "NA", "LATAM"];
  var usernames = ["morgane_unilever_api", "morgane_unilever3_api" ,"morgane_unilever2_api", "morgane_unileverlatam_api"]

  
  for (u =0; u<4; u++) {
   var options = {'method':'post', 
                  'payload' : 'grant_type=password&username='+usernames[u]+'&password='+password,
                  'headers': {'Authorization':'Basic dW5pbGV2ZXItYmFhYW06bmw0ZTRnNWhuYjd2Z2RocnI3MWxtamRlMnJzaHFvbWV2cDlxbW1vcHZwbnNmcDJhZnUz',
                              'Content-Type' : 'application/x-www-form-urlencoded' 
                             }
                 }

   var response = JSON.parse( UrlFetchApp.fetch("https://api.demdex.com/oauth/token",
                      options) )
  
   tokens[regions[u]] = response.access_token;
}
  
  var scriptProperties  = PropertiesService.getScriptProperties ();
  scriptProperties.setProperty('EMEAaccessToken', tokens.EMEA);
  scriptProperties.setProperty('APACaccessToken', tokens.APAC);
  scriptProperties.setProperty('NAaccessToken', tokens.NA);
  scriptProperties.setProperty('LATAMaccessToken', tokens.LATAM);
 
  return tokens;

  
}
