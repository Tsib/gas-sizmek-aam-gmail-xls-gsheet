function onOpen() {
  
var ui = SpreadsheetApp.getUi();
ui.createMenu('UC Automation - Sizmek')
.addSubMenu( ui.createMenu('Update References').addItem("Update Folder Refs", "getFolders").addItem("Update Data Source Refs", "getDSs") )
.addSeparator()
.addSubMenu( ui.createMenu('AAM Automation')
            .addItem("Manually trigger daily feed processing", 'sizmekAutomation')
            .addSeparator()
            .addItem("Manually process current daily feed sheet", "processCampaigns")
           )

                .addToUi();  
}

