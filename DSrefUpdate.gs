//function to pull updated list of Data Source IDs for references across all instances
function getDSs() {
  
  var ss = SpreadsheetApp.getActive().getSheetByName("Data Source Lookups");
  var row = 1;
  ss.getRange(2, 1, 10000, 7).clearContent();
  
  //get AAM API tokens for all regions
  var tokens = getAAMtokens();
  var regions = ["NA", "EMEA", "APAC", "LATAM"];
  
  for (r in regions) { 
  
  var options = {'method':'get',
                     'headers': {'Authorization':'Bearer ' + tokens[regions[r]], 'Content-Type': 'application/json'},
                     'muteHttpExceptions':true } 
      response = UrlFetchApp.fetch("https://api.demdex.com/v1/datasources?search=site&excludeReportSuites=true&sortBy=name", options) 
      var responseCode= parseInt( response.getResponseCode() );
      var APIresponse = response.getContentText();
      var datasources = JSON.parse(response);
      row++
    
    for (d in datasources) {
  
      var market = datasources[d].name.split(" - ")[0];
      var onOffSite = datasources[d].name.split(" - ")[1];
       Logger.log(market + " : " + row)
      //move on to next row if it's a new market (data is sorted by DS name)
      if (d > 0 && market !== datasources[d-1].name.split(" - ")[0]) {
      row++; 
      }
      if (typeof(onOffSite) == "undefined") {
       continue; 
      } else if ( onOffSite.match( "Offsite Data") !== null ) {
      ss.getRange(row, 1).setValue(regions[r]);
      ss.getRange(row, 2).setValue(market);
      ss.getRange(row, 3).setValue(datasources[d].dataSourceId);
      } else if (onOffSite.match( "Onsite Data") !== null ) {
        ss.getRange(row, 1).setValue(regions[r]);
        ss.getRange(row, 2).setValue(market);
        ss.getRange(row, 4).setValue(datasources[d].dataSourceId);
      }
     
      
      //close datasource loop
    }
      
    //close regions loop
  }
    
}
