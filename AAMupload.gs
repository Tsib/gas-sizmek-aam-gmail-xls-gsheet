function processCampaigns(ssId, e) {
  
  //get sheet ID to process from current sheet if function triggered through the menu, not from schedule.
  if (typeof(ssId) == "undefined") { ssId = SpreadsheetApp.getActive().getId(); }
  
  //master log for trait creation and error auditing
  masterLog = SpreadsheetApp.openById("1LyDRWcrAMWqj8-MarckmS1BAcHtvOqn5iHVb8aZrvk0");
  successLog = masterLog.getSheetByName("SuccessLog");
  sLrows = successLog.getLastRow();
  sLrows2 = sLrows; //used to increment success rows on log sheet
  errorLog = masterLog.getSheetByName("ErrorLog");
  eLrows = errorLog.getLastRow(); //used to remember first error row of the day for email content
  eLrows2 = eLrows; //used to increment error rows on log sheet
  errors = 0;
  successes = 0;
  
  //use for manual testing
  //var ssId = "SHEETID";
   var SS = SpreadsheetApp.openById(ssId);
   ss = SS.getSheets()[0];
   rows = ss.getLastRow();
  //cancel execution if no campaign rows are in the file (for feeds from the weekend)
  if (rows == 1) {return}
  //copy master AAM Folder and Data Source lookup tables
  if ( SS.getSheetByName("Folder lookups") == null)  { masterLog.getSheetByName("Folder lookups").copyTo(SS).setName("Folder lookups"); }
  if ( SS.getSheetByName("Data Source Lookups") == null) { masterLog.getSheetByName("Data Source Lookups").copyTo(SS).setName("Data Source Lookups"); }
  //write headers for script output
  ss.getRange("L1:AD1").setValues([["Country","Region","Display Folder Name","Display Folder ID","DataSource ID","Imp. Trait Name","Imp. Trait Rule","Imp. Trait Integration Code","Imp. Trait Description","Imp. Trait Comment","Imp. API Response Code","Imp. API Response","Click Trait Name","Click Trait Rule","Click Trait Integration Code","Click Trait Description","Click Trait Comment","Click API Response Code","Click API Response"]]);
  
  //get AAM API tokens for all regions
  var tokens = getAAMtokens();
  //extract country from Account name in column L
  ss.getRange(2, 12, rows-1, 1).setFormulaR1C1('=MID(R[0]C[-9],FIND(" ",R[0]C[-9])+1,3)');
  //enter vlookup for region name in column M
  ss.getRange(2, 13, rows-1, 1).setFormula("=vlookup(R[0]C[-1],'Folder lookups'!$A$2:$B$2000, 2,FALSE)");
  //enter vlookup for country-brand-Display folder ID. In two steps because using R1C1 notation in nested formulas doesnt play nice
  ss.getRange(2, 14, rows-1, 1).setFormula('=CONCATENATE( R[0]C[-2] ,"_", R[0]C[-9] )');
  ss.getRange(2, 15, rows-1, 1).setFormula("=vlookup(R[0]C[-1], 'Folder lookups'!$D$2:$F$2000, 3, FALSE) ");
  //enter vlookup formula for Data Source ID
  ss.getRange(2, 16, rows-1, 1).setFormula("=VLOOKUP( R[0]C[-4],'Data Source Lookups'!$B$2:$C$500, 2, FALSE)");
  
  
  for ( r=2; r<=rows; r++) {
  
  var brand = ss.getRange(r, 5).getValue();
  var market = ss.getRange(r, 12).getValue();
  var region = ss.getRange(r, 13).getValue();
  var folderId = ss.getRange(r, 15).getValue();
  var dataSourceId = ss.getRange(r, 16).getValue();
  accessToken = tokens[region];

  var campaignName = ss.getRange(r, 7).getValue();
  var campaignId = ss.getRange(r, 6).getValue();
  var previousCampaignId = ss.getRange(r-1, 6).getValue();
  // skip to next row if this row doesn't contain a new campaign OR if this campaign cannot be linked to an AAM folder (eg Sizmek account does not correspond to valid UL brand or brand not set up in AAM)  
  if (folderId == "#N/A" || campaignId == previousCampaignId) {continue;}
    
  var trackingType = ss.getRange(r, 11).getValue();
     
  traitDefinitionView = buildTrait(region, market, brand, campaignName, campaignId, dataSourceId, folderId, accessToken, "Click") ;
  traitDefinitionView = buildTrait(region, market, brand, campaignName, campaignId, dataSourceId, folderId, accessToken, "Imp") ; 
    
  }

  //send results email notification if there were errors or successes 
  errors == 0 ? errorResults = [] : errorResults = errorLog.getRange(eLrows+1, 1, eLrows2 - eLrows, 8).getValues()  ;
  successes == 0 ? successResults = [] : successResults = successLog.getRange(sLrows+1, 1, sLrows2 - sLrows, 9).getValues() ;
  //var errorResults = errorLog.getRange(eLrows+1, 1, eLrows2 - eLrows, 7).getValues();
  //var successResults = successLog.getRange(sLrows+1, 1, sLrows2 - sLrows, 7).getValues();
  if (errors > 0 || successes > 0) {
  emailResults(errors, successes, errorResults, successResults, e);
  }
}

    
    function buildTrait( region, market, brand, campaignName, campaignId, dataSourceId, folderId, accessToken, event ) {
     
      //write Trait Creation results in columns Q-W for the Impression trait, or offset to columns X-AD for Click
      if (event == "Click") {
      var ruleEvent = "click"
      var c = 17;
      } else {
        var ruleEvent = "imp"
        var c = 24;
        } 
      
      //define trait details, use helper function to parse Campaign Name into elements based on market's taxonomy
       var parsedCampaign = parseCampaignName(region, market, brand, campaignName);
      
      //skip this campaign if there is no declared naming convention in place
      if (parsedCampaign == false) {
      return;
      } else if (parsedCampaign == "ERROR") {
        ss.getRange(r, c).setValue( "Error encountered - unsupported campaign name." );
        eLrows2++
        errors++
        errorLog.getRange(eLrows2, 1).setValue(new Date() );
        errorLog.getRange(eLrows2, 2).setValue(region);
        errorLog.getRange(eLrows2, 3).setValue(brand);
        errorLog.getRange(eLrows2, 4).setValue(market);
        errorLog.getRange(eLrows2, 5).setValue(campaignName);
        errorLog.getRange(eLrows2, 6).setValue(campaignId);
        errorLog.getRange(eLrows2, 7).setValue("Error encountered - unsupported campaign name."  );
      }
      
       traitName =  parsedCampaign[0] + event +  parsedCampaign[1];
       traitDescription = event + "s for the campaign '" +campaignName+ '"';
       traitIntegrationCode = campaignId+"_"+event;
       traitComments = "Trait automatically created by Sizmek-AAM Automation tool.";
       traitRule = 'd_campaign == "' + campaignId + '" AND d_event == "' +ruleEvent+ '" AND (c_country == "' +market+ '" OR d_country == "' +market+ '")';

      
       //check whether Trait for Creative already exists by Integration Code, skip creation if so 
      var IClookupResponse = searchTrait(traitIntegrationCode);
      if (IClookupResponse == 200) { 
        ss.getRange(r, c).setValue( "Trait with the same Integration Code already exists, skipping campaign." );
        return;
      } 

      
      var traitDefinition = {
        "name": traitName,
        "description": traitDescription,
        "integrationCode": traitIntegrationCode,
        "comments": traitComments,
        "traitType": "RULE_BASED_TRAIT",
        "status": "ACTIVE",
        "dataSourceId": dataSourceId,
        "folderId": folderId,
        "traitRule": traitRule,
        "categoryId": 0,
        "ttl": "720"
        // ,"type": 1,
        //"pid": 0,
        //"crUID": 0,
        //"upUID": 0,
        //"createTime": 0,
        //"updateTime": 0,
        //"algoModelId": 0,
        //"thresholdValue": 0,
        //"thresholdType": "ACCURACY"
      }
      
   
      var options = {'method':'post',
                     'payload': JSON.stringify(traitDefinition),
                     'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                     'muteHttpExceptions':true } 
      response = UrlFetchApp.fetch("https://api.demdex.com/v1/traits",
                                       options) 
      var responseCode= parseInt( response.getResponseCode() );
      var APIresponse = response.getContentText();
      
      ss.getRange(r, c).setValue( traitDefinition.name );
      ss.getRange(r, c+1).setValue( traitDefinition.traitRule );
      ss.getRange(r, c+2).setValue( traitDefinition.integrationCode );
      ss.getRange(r, c+3).setValue( traitDefinition.description );
      ss.getRange(r, c+4).setValue( traitDefinition.comments );
      ss.getRange(r, c+5).setValue(responseCode);
      ss.getRange(r, c+6).setValue(APIresponse);
    
      //record results in the separate Master Sizmek-AAM automation audit log
      if (responseCode > 200 && responseCode < 300 ) {
        sLrows2++
        successes++  
        successLog.getRange(sLrows2, 1).setValue(new Date() );
        successLog.getRange(sLrows2, 2).setValue(region);
        successLog.getRange(sLrows2, 3).setValue(brand);
        successLog.getRange(sLrows2, 4).setValue(market);
        successLog.getRange(sLrows2, 5).setValue(campaignName);
        successLog.getRange(sLrows2, 6).setValue(campaignId);
        successLog.getRange(sLrows2, 7).setValue(traitDefinition.name);
        successLog.getRange(sLrows2, 8).setValue("https://bank.demdex.com/portal/Traits/Traits.ddx#view/"+JSON.parse(APIresponse).sid);
        successLog.getRange(sLrows2, 9).setValue(APIresponse);
      //record errors in the Master Sizmek-AAM automation audit log, if there was an error in creating the trait, NOT for duplicate trait exceptions
      } else if ( JSON.parse(APIresponse).code !== "RULE_EXISTS")  {
        eLrows2++
        errors++
        errorLog.getRange(eLrows2, 1).setValue(new Date() );
        errorLog.getRange(eLrows2, 2).setValue(region);
        errorLog.getRange(eLrows2, 3).setValue(brand);
        errorLog.getRange(eLrows2, 4).setValue(market);
        errorLog.getRange(eLrows2, 5).setValue(campaignName);
        errorLog.getRange(eLrows2, 6).setValue(campaignId);
        errorLog.getRange(eLrows2, 7).setValue(traitDefinition.name);
        errorLog.getRange(eLrows2, 8).setValue(APIresponse);
      }
     
     
    }

//function used to check whether a trait was already created by the automation tool 
// for situations where a trait was created by automation but manually edited in AAM (eg Celtra campaign, keep trait but replace rules), so a duplicate would be created with a different rule
function searchTrait(traitIntegrationCode) {

    var options = {'method':'get',
      'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response =  UrlFetchApp.fetch("https://api.demdex.com/v1/traits/ic:" + traitIntegrationCode,
                                 options)  
    
    var responseCode = response.getResponseCode();
    return responseCode;

}
