//this file contains the function to parse a Sizmek Campaign name into an AAM Trait name based on the Naming convention used in the market

//campaign level trait
function parseCampaignName(region, market, brand, SZcampaignName) {
  
  var fragments = "";
  var campaign = "";
  var launch = "";
  var objective = "";
  var code = "";
  
  if (region == "LATAM") {
  //case for Cadreon naming convention across LATAM
    
    fragments = SZcampaignName.split("|");
    campaign = "";
    //return nothing until naming convention is coded in
    return false;
    
  } else if (region == "APAC") {
    //Case for MS naming convention in APAC markets
    fragments = SZcampaignName.split("_");
    campaign = "";
    //return nothing until naming convention is coded in
    return false;
    
  } else if ( market.match("DE|CH|AT") !== null) {
    //case for MS naming convention from the DRAW generator tool in Europe 
    fragments = SZcampaignName.split("_");
    //determine whether it's a global campaign in which case there is an extra position for the L_ at the start because whoever made this convention IS A FLIPPING GENIUS
    (fragments[0] == "L" ) ? offset = 1 : offset = 0;
    campaign = fragments[0 + offset];
    launch = fragments[1 + offset ];
    //if Month or Year or missing, remove "undefined" from the trait name
    launch = launch.replace("undefined","");
    code = fragments[2 + offset ];
  
  } else if (market == "US") {
    //wrap the US code in a try/catch because they are very naughty with their names and the code might encounter a fatal error that halts processing of other markets
    try{
      //case for US MS naming convention as agreed with Abby Free on March 6th: CLIENT_BRAND_CAMPAIGN ID_ CAMPAIGN NAME_YEAR_MONTH
      fragments = SZcampaignName.split("_");
      campaign = fragments[3];
      launch = toString(fragments[5]+fragments[4]); //add Month and Year to make MMMYYYY
      //if Month or Year or missing, remove "undefined" from the trait name
      launch = launch.replace("undefined","");
      launch = launch.replace("object Undefined","");
      launch = launch.replace("[object Object]","");
    } catch(err) {
      return false
    }
  } else { //if no naming convention is matched
    return "ERROR";
  }
  
  //as an array to insert the Click/Impression piece in the middle
  var traitName = [market + "_" + brand + "_DisplayStatic_","_Campaign_" + campaign + " " + launch];
  return traitName;
  
}
