//function to pull updated list of brand Display and Social folder IDs for references across all instances
function getFolders() {
  
  var ss = SpreadsheetApp.getActive().getSheetByName("Folder lookups");
  var row = 2;
  ss.getRange(2, 1, 10000, 7).clearContent();
  
  //get AAM API tokens for all regions
  var tokens = getAAMtokens();
  var regions = ["EMEA", "APAC", "NA", "LATAM"];
  
  for (r in regions) { 
  
  var options = {'method':'get',
                     'headers': {'Authorization':'Bearer ' + tokens[regions[r]], 'Content-Type': 'application/json'},
                     'muteHttpExceptions':true } 
      response = UrlFetchApp.fetch("https://api.demdex.com/v1/folders/traits", options) 
      var responseCode= parseInt( response.getResponseCode() );
      var APIresponse = response.getContentText();
  
  
  var ULfolder = JSON.parse(response)[0].subFolders[0];
  
  for (m in ULfolder.subFolders) {
    
    for (b in ULfolder.subFolders[m].subFolders) {
    
      for (f in ULfolder.subFolders[m].subFolders[b].subFolders) {
    
        ss.getRange(row, 1).setValue(ULfolder.subFolders[m].name);
        ss.getRange(row, 2).setValue(regions[r]);
        ss.getRange(row, 3).setValue(ULfolder.subFolders[m].folderId);
        ss.getRange(row, 4).setValue(ULfolder.subFolders[m].subFolders[b].name);
        ss.getRange(row, 5).setValue(ULfolder.subFolders[m].subFolders[b].folderId);
        if (ULfolder.subFolders[m].subFolders[b].subFolders[f].name == "Display") {
        ss.getRange(row, 6).setValue(ULfolder.subFolders[m].subFolders[b].subFolders[f].folderId);
        } else if (ULfolder.subFolders[m].subFolders[b].subFolders[f].name == "Social") {
        ss.getRange(row, 7).setValue(ULfolder.subFolders[m].subFolders[b].subFolders[f].folderId);
        }
     //close brand subfolders loop
      }
      //increment row, close brand loop
        row++; 
    }
       //close market loop
  }
   
  //close regions loop
  }
  
    
}
