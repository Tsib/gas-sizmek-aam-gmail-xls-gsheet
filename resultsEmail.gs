//function to send email alerts with all daily errors to global team and the relevant market errors to local teams
function emailResults(errors, successes, errorResults, successResults, e) {
  
  var localErrors = 0;  
  var ss = SpreadsheetApp.getActive().getSheetByName("Email Alert contacts");
  var rows = ss.getLastRow()
  var log = {};
  var contactsList = {};
  var contacts = ss.getRange(2, 1, rows-1, 2).getValues();
  
  //enter contacts in lookup object
  for (c in contacts) {
    contactsList[contacts[c][0]] = contacts[c][1];
       }

    
  body = HtmlService.createHtmlOutputFromFile('successesEmailBody.html');
  var globalBody = body;
  
  
  //write success results to email html table
  for (s=0; s<successResults.length; s++) {
  
    var market = successResults[s][3];
    
    if ( typeof(log[market]) !== "object")  {
    log[market] = {};
    log[market].name = market;
    log[market].body = HtmlService.createHtmlOutputFromFile('successesEmailBody.html');
    log[market].successCount = 0;   
    log[market].errorCount = 0; 
    }
    
    log[market].successCount++;
    var addedSuccess =  '<tr>'
    +'<td style="border:1px solid black">'+successResults[s][0]+'</td>'
    +'<td style="border:1px solid black">'+successResults[s][1]+'</td>'
    +'<td style="border:1px solid black">'+successResults[s][2]+'</td>'
    +'<td style="border:1px solid black">'+successResults[s][3]+'</td>'
    +'<td style="border:1px solid black">'+successResults[s][4]+'</td>'
    +'<td style="border:1px solid black">'+successResults[s][5]+'</td>'
    +'<td style="border:1px solid black">'+successResults[s][6]+'</td>'
    +'<td style="border:1px solid black">'+successResults[s][7]+'</td>'
    +'<td style="border:1px solid black">'+successResults[s][8]+'</td>'
  +'</tr>'
      
  log[market].body.append(addedSuccess);
    globalBody.append(addedSuccess);
  
}
   var errorsHead = ' <p>&nbsp;</p>   <h2>Trait creation Error log</h2>'
   +'<p>This is an automated alert. Do not reply to this email</p>'
+ '<table style="width:100%;border-collapse:collapse;align:left">'
  + '<tr>'
    + '<th style="border:1px solid black;">DateTime</th>'
    + '<th style="border:1px solid black;">Region</th>' 
    + '<th style="border:1px solid black;">Advertiser</th>'
    + '<th style="border:1px solid black;">Market</th>'
    + '<th style="border:1px solid black;">Campaign Name</th>'
    + '<th style="border:1px solid black;">Campaign ID</th>'
    + '<th style="border:1px solid black;">Trait Name</th>'
    + '<th style="border:1px solid black;">AAM Response</th>'
  + '</tr>';

    log[market].body.append('</table>' + errorsHead);
    globalBody.append('</table>' + errorsHead);
    
  //write error results to email html table 
  for (e=0; e<errorResults.length; e++) {
  
    var market = errorResults[e][3];
    
    if ( typeof(log[market]) !== "object")  {
    log[market] = {};
    log[market].name = market;
    log[market].body = HtmlService.createHtmlOutputFromFile('errorsEmailBody.html');
    log[market].errorCount = 0;   
    }
    
    log[market].errorCount++;
    var addedError =  '<tr>'
    +'<td style="border:1px solid black">'+errorResults[e][0]+'</td>'
    +'<td style="border:1px solid black">'+errorResults[e][1]+'</td>'
    +'<td style="border:1px solid black">'+errorResults[e][2]+'</td>'
    +'<td style="border:1px solid black">'+errorResults[e][3]+'</td>'
    +'<td style="border:1px solid black">'+errorResults[e][4]+'</td>'
    +'<td style="border:1px solid black">'+errorResults[e][5]+'</td>'
    +'<td style="border:1px solid black">'+errorResults[e][6]+'</td>'
    +'<td style="border:1px solid black">'+errorResults[e][7]+'</td>'
  +'</tr>'
  
    log[market].body.append(addedError);
    globalBody.append(addedError);
      
}

//compose error log email for each market
for (m in Object.keys(log)) {
  
  //if the script has been ran manually then skip the market emails. If ran by the daily time trigger, continue to generate them.
  //  if(typeof(e) == "undefined") {break}
     
  var marketObj = log[Object.keys(log)[m]]

      var draft = GmailApp.createDraft(contactsList[marketObj.name], "Sizmek Automation "+marketObj.name+": "+marketObj.successCount+ " Successes | " +marketObj.errorCount+ " Errors", "", {htmlBody: marketObj.body.getContent()+'</table>' });
      draft.send(); 
     }
  
  //email to Global D+A with all market errors     
  var draftGlobal = GmailApp.createDraft(contactsList.global, "Sizmek Automation Global: "+successes+ " Successes | " +errors+ " Errors", "", {htmlBody: globalBody.getContent()+'</table>' });
  draftGlobal.send();
 
  
}
