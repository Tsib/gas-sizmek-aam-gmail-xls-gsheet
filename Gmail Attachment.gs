function sizmekAutomation(e) {

  //run function to get latest Sizmek email and grab xls attachment
  var attachment = getAttachment();
  //run function to convert to Gsheet and upload to Drive 
  var ssId = uploadSheet(attachment);
  //run function to create Traits in AAM off sheet campaigns/placements
  processCampaigns(ssId, e);
 
}

function getAttachment() {
  
  //search for Sizmek feed thread based on subject
  var threads = GmailApp.search("Data Feed from MediaMind", 0, 1)
  //get latest thread
  var messages = threads[0].getMessages();
  
  //get latest message
  var email = messages[messages.length-1];
  
  //get the ZIP attachment
  var attachments = email.getAttachments();  
  for (a=0; a<attachments.length; a++) {
    
    
    //ADD CODE TO CHECK EMAIL DATE IS TODAY IN CASE SIZMEK EMAIL HAS NOT ARRIVED YET
    
    
    if (attachments[a].getName().match("zip") !== null) {
      //unzip
      var zip = attachments[a].setContentTypeFromExtension() ;
      var unzipped = Utilities.unzip(zip);
      return unzipped[0];
    }
       }
}


function uploadSheet(attachment) {

  //get Sizmek-AAM Automation folder to upload GSheet
  var folder = DriveApp.getFolderById("1mEk2vWzyZhlUJuTdvinYTb-1TRPEM_LA");
  
 //use Drive API Advanced Service in order to do the xlsx->GSheet conversion

    var blob = attachment.copyBlob();
    var resource = {
      title: attachment.getName(),
      parents:[{id:"1mEk2vWzyZhlUJuTdvinYTb-1TRPEM_LA"}]
    };
    var ssId = Drive.Files.insert(resource, blob, {
      convert: true
    }).id;
  
  return ssId;  

}



  
  
